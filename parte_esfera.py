import sys, pygame
# Inicializamos pygame
pygame.init()
# Muestro una ventana de 800x600
size = 800, 550
screen = pygame.display.set_mode(size)
# Cambio el título de la ventana
pygame.display.set_caption("Juego Billar")
# Inicializamos variables
width, height = 800, 550
speed = [1, 1]
white = 255, 255, 255
# Crea un objeto imagen y obtengo su rectángulo
#ball = pygame.image.load("bola_bies.jpg")
ball2 = pygame.image.load("bola_7.jpg")
ball2rect = ball2.get_rect()
#ballrect = ball.get_rect()
# Comenzamos el bucle del juego
run=True
while run:
	# Espero un tiempo (milisegundos) para que la pelota no vaya muy rápida
	pygame.time.delay(2)
	# Capturamos los eventos que se han producido
	for event in pygame.event.get():
		#Si el evento es salir de la ventana, terminamos
		if event.type == pygame.QUIT: run = False
	# Muevo la pelota
	# AGREGO OTRA BOLA EN LA MESA
	ball2rect = ball2rect.move(speed)
	#ballrect = ballrect.move(speed)
	#comprueba si la pelota llega a las lineas de la ventana
	if ball2rect.left < 0 or ball2rect.right > width:
		speed[0] = -speed[0]
	if ball2rect.top < 0 or ball2rect.bottom > height:
		speed[1]=-speed[1]

	# PELOTA 2

	#Pinto el fondo de blanco, dibujo la pelota y actualizo la pantalla
	screen.fill(white)
	screen.blit(ball2,ball2rect)
	pygame.display.flip()
# Salgo de pygame
pygame.quit()
