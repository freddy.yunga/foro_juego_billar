import sys, pygame
# Inicializamos pygame
pygame.init()
# Muestro una ventana de 800x600
size = 800, 600
screen = pygame.display.set_mode(size)
# Cambio el título de la ventana
pygame.display.set_caption("Juego BILLA")
# Inicializamos variables
width, height = 800, 600
speed = [1, 1]
#white = 255, 255, 255
#green = 0, 255,0
whitebate=255,255,255

# Crea un objeto imagen pelota y obtengo su rectángulo
ball = pygame.image.load("bola_bies.jpg")
ballrect = ball.get_rect()
# Crea un objeto imagen bate y obtengo su rectángulo
bola_2 = pygame.image.load("Bate.jpg")
bola_2rect = bola_2.get_rect()
# Pongo otro bola en el centro de la pantalla
bola_2rect.move_ip(400,260)
# Comenzamos el bucle del juego
run=True
while run:
	# Espero un tiempo (milisegundos) para que la pelota no vaya muy rápida
	pygame.time.delay(1)
	# Capturamos los eventos que se han producido
	for event in pygame.event.get():
		#Si el evento es salir de la ventana, terminamos
		if event.type == pygame.QUIT: run = False
	# Compruebo si se ha pulsado alguna tecla
	keys = pygame.key.get_pressed()
	if keys[pygame.K_UP]:
		bola_2rect=bola_2rect.move(0, -1)
	if keys[pygame.K_DOWN]:
		bola_2rect=bola_2rect.move(0, 1)
	# aqui trato de comprobar si hay colisión
	if bola_2rect.colliderect(ballrect):
		speed[0] = - speed[0]
	# Muevo la pelota
	ballrect = ballrect.move(speed)
	# verifico si la pelota llega a los limites de la ventana
	if ballrect.left < 0 or ballrect.right > width:
		speed[0] = -speed[0]
	if ballrect.top < 0 or ballrect.bottom > height:
		speed[1] = -speed[1]
	#Pinto el fondo de blanco, dibujo la pelota y actualizo la pantalla
	#screen.fill(green)
	#screen.fill(white)
	screen.fill(whitebate)
	screen.blit(ball, ballrect)
	# DIBUJO MI BOLA 2
	screen.blit(bola_2, bola_2rect)
	pygame.display.flip()
# Salgo de pygame
pygame.quit()